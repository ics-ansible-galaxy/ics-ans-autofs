import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('autofs')


def test_autofs_running_and_enabled(host):
    service = host.service("autofs")
    assert service.is_enabled
    assert service.is_running
